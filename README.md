# About

This project is an experimental interface I used to run a study for my Honors thesis at Baylor University. It renders a sign-up for a fake social media service and a few different kinds of privacy policy presentations, then quizzes the user on the privacy policy. Data on the user's answers is stored for later analysis.

# Setup

## Layer 1: Database

Download [MariaDB](https://mariadb.org/). Fire it up and create a database called "thesis." If you have a password set for the root user or want to use a different configuration, you can modify `application.properties` (src -> main -> resources).

## Layer 2: Back-End

Ensure that you have Java 11 installed. The project uses [Spring Boot](https://spring.io/projects/spring-boot), a Java framework that makes use of annotations to automatically handle many aspects of the application. It also uses [Gradle](https://gradle.org/) to manage depdencies. It will help you immensely to open the project in an IDE that has plugins for both (I use [IntelliJ](https://www.jetbrains.com/idea/)), Once you have the project set up, go ahead and run it (the main class is `Application.java`). You should see some Spring output - you'll know it's loaded when it preloads the database with the experiment questions.

## Layer 3: Front-End

Open a terminal window in the `client` directory and run `npm install`. You'll also want to download the [Angular CLI](https://cli.angular.io/). Once you're set, run `ng serve --open`. The consent form should open in a new window in your default browser.