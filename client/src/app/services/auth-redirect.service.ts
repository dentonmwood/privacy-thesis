import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {AppState} from "../app.state";
import {Store} from "@ngrx/store";

@Injectable({
  providedIn: 'root'
})
export class AuthRedirectService implements CanActivate {

  constructor(
    private router: Router,
    private store: Store<AppState>
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    let retval = true;
    this.store.select('user').subscribe(user => {
      if(user.id == -1) {
        this.router.navigate(["/error/bad-access"]);
        retval = false;
      }
    })
    return retval;
  }
}
