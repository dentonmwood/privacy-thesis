import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "../models/user";
import {Observable, of} from "rxjs";
import {catchError} from "rxjs/operators";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private usersUrl = '//and.ecs.baylor.edu:8080/users';

  constructor(private http: HttpClient) { }

  addUser(user: User): Observable<User> {
    return this.http.post<User>(this.usersUrl, user, httpOptions)
      .pipe(
        catchError(this.handleError('addUser'))
      );
  }

  updateUser(user: User): Observable<User> {
    let url = this.usersUrl + "/" + user.id;
    let payload = this.buildPostRequest(user);
    let response = this.http.post<User>(url, payload, httpOptions);
    response.subscribe(user => console.log("Thanks!"));
    return response;
  }

  buildPostRequest(user) {
    let answers = [];
    for (let answer in user.answers) {
      answers.push(user.answers[answer].id);
    }
    return {id: user.id, answers: answers};
  }

  handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }
}
