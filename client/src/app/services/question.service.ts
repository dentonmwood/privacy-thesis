import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {Observable} from "rxjs";
import {Question} from "../models/question";
import {RadioQuestion} from "../models/form/radio-question";
import {catchError, map} from "rxjs/operators";
import {of} from "rxjs/internal/observable/of";
import {Answer} from "../models/answer";
import {QuestionBase} from "../models/form/question-base";

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  private questionsUrl = '//and.ecs.baylor.edu:8080/questions';

  constructor(private http: HttpClient) { }

  getQuestions(): Observable<QuestionBase<any>[]> {
    return this.http.get<Question[]>(this.questionsUrl).pipe(
      map(res => res.map(question => new RadioQuestion({
        key: question.id + 1,
        label: question.text,
        type: 'radio',
        options: this.getAnswers(question),
        required: true,
        order: 1
      })) ,
      catchError(this.handleError<Question[]>('getServerQuestions'))
    ))
  }

  private getAnswers(question: Question): Array<Answer> {
    let answer: Answer;
    let answers = [];

    for (answer of question.answers) {
      answers.push({
        id: answer.id,
        text: answer.text,
        correct: answer.correct
      });
    }

    return answers;
  }

  handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }

}
