import { Component, OnInit } from '@angular/core';

export const videoURL = 'https://youtu.be/74tQEjTUfSo';

  @Component({
  selector: 'app-video-privacy-policy',
  templateUrl: './video-privacy-policy.component.html',
  styleUrls: ['./video-privacy-policy.component.css']
})
export class VideoPrivacyPolicyComponent {
  player: YT.Player;
  private id: string = '74tQEjTUfSo';

  savePlayer(player) {
    this.player = player;
  }

  onStateChange(event) {
    console.log('player state', event.data);
  }
}
