import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoPrivacyPolicyComponent } from './video-privacy-policy.component';

describe('VideoPrivacyPolicyComponent', () => {
  let component: VideoPrivacyPolicyComponent;
  let fixture: ComponentFixture<VideoPrivacyPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoPrivacyPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoPrivacyPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
