import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextPrivacyPolicyComponent } from './text-privacy-policy.component';

describe('TextPrivacyPolicyComponent', () => {
  let component: TextPrivacyPolicyComponent;
  let fixture: ComponentFixture<TextPrivacyPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextPrivacyPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextPrivacyPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
