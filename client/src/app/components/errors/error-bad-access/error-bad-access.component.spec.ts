import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorBadAccessComponent } from './error-bad-access.component';

describe('ErrorBadAccessComponent', () => {
  let component: ErrorBadAccessComponent;
  let fixture: ComponentFixture<ErrorBadAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorBadAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorBadAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
