import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorRepeatUserComponent } from './error-repeat-user.component';

describe('ErrorRepeatUserComponent', () => {
  let component: ErrorRepeatUserComponent;
  let fixture: ComponentFixture<ErrorRepeatUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorRepeatUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorRepeatUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
