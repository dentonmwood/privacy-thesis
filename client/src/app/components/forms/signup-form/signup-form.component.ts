import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../app.state";
import {Observable} from "rxjs";
import {User} from "../../../models/user";

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {
  user: Observable<User>;
  trialNum: number;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.user = this.store.select('user');
    this.getTrialNum();
  }

  getTrialNum() {
    this.user.subscribe(user => {
      this.trialNum = user[1].trialNum;
    });
  }
}
