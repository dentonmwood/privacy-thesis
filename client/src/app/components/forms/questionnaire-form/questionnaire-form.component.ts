import {Component, OnInit} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {QuestionBase} from "../../../models/form/question-base";
import {QuestionControlService} from "../../../services/question-control.service";
import {QuestionService} from "../../../services/question.service";
import {UserService} from "../../../services/user.service";
import {Answer} from "../../../models/answer";
import {RadioQuestion} from "../../../models/form/radio-question";
import {Observable} from "rxjs";
import {User} from "../../../models/user";
import {AppState} from "../../../app.state";
import {Store} from "@ngrx/store";

@Component({
  selector: 'app-question-form',
  templateUrl: './questionnaire-form.component.html',
  styleUrls: ['./questionnaire-form.component.css'],
  providers: [ QuestionControlService ]
})
export class QuestionnaireFormComponent implements OnInit {
  questions: QuestionBase<any>[];
  questionForm: FormGroup;
  answers: Array<Answer>;
  user: Observable<User>;

  constructor(
    private questionControlService: QuestionControlService,
    private questionService: QuestionService,
    private userService: UserService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.questionService.getQuestions().subscribe(questions => {
      this.questions = questions;
      this.questionForm = this.questionControlService.toFormGroup(questions);
      this.user = this.store.select('user');
    })
  }

  onSubmit() {
    this.answers = [];
    let questionId;
    let i = 0;
    for (questionId in this.questionForm.value) {
      let answerId = this.questionForm.value[i + 1].id;
      if (this.questions[i] instanceof RadioQuestion) {
        let answer = this.questions[i].options[answerId];
        this.answers.push(new Answer(
          answer.id.id,
          answer.text,
          answer.correct,
          answer.id.questionId
        ));
      }
      i++;
    }
    this.saveAnswers();
  }

  saveAnswers() {
    this.user.subscribe(user => {
      let userToPut = user[1];
      userToPut.answers = this.answers;
      this.userService.updateUser(userToPut);
    })
  }

}
