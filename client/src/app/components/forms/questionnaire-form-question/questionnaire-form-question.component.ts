import {Component, Input, OnInit} from '@angular/core';
import {QuestionBase} from "../../../models/form/question-base";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-questionnaire-form-question',
  templateUrl: './questionnaire-form-question.component.html',
  styleUrls: ['./questionnaire-form-question.component.css']
})
export class QuestionnaireFormQuestionComponent {

  @Input() question: QuestionBase<any>;
  @Input() questionForm: FormGroup;

  get isValid() { return this.questionForm.controls[this.question.key].valid; }
}
