import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireFormQuestionComponent } from './questionnaire-form-question.component';

describe('QuestionnaireFormQuestionComponent', () => {
  let component: QuestionnaireFormQuestionComponent;
  let fixture: ComponentFixture<QuestionnaireFormQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionnaireFormQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireFormQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
