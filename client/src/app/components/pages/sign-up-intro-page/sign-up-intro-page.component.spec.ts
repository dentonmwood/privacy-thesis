import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpIntroPage } from './sign-up-intro-page.component';

describe('SignUpIntroPage', () => {
  let component: SignUpIntroPage;
  let fixture: ComponentFixture<SignUpIntroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpIntroPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpIntroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
