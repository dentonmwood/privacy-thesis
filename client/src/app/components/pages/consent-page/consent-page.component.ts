import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../services/user.service";
import {User} from "../../../models/user";
import {AppState} from "../../../app.state";
import {Store} from "@ngrx/store";
import * as UserActions from "../../../actions/user.actions"

@Component({
  selector: 'app-consent-form',
  templateUrl: './consent-page.component.html',
  styleUrls: ['./consent-page.component.css']
})
export class ConsentPageComponent implements OnInit{
  consent: boolean;

  constructor(
    private userService: UserService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
  }

  onClick() {
    this.userService.addUser(new User())
      .subscribe(user => {
        this.store.dispatch(new UserActions.AddUser(user));
        this.store.select('user').subscribe(user => {
          this.userService.addUser(user);
        })
      }
    );
  }
}
