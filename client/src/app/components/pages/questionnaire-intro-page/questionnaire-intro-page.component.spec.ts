import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireIntroPageComponent } from './questionnaire-intro-page.component';

describe('QuestionnaireIntroPageComponent', () => {
  let component: QuestionnaireIntroPageComponent;
  let fixture: ComponentFixture<QuestionnaireIntroPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionnaireIntroPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireIntroPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
