import { Component, OnInit } from '@angular/core';
import {AppState} from "../../../app.state";
import {Store} from "@ngrx/store";

@Component({
  selector: 'app-debrief',
  templateUrl: './debrief-page.component.html',
  styleUrls: ['./debrief-page.component.css']
})
export class DebriefPageComponent implements OnInit {
  trialNum: number;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.select('user').subscribe(user => {
      this.trialNum = user[1].trialNum;
    })
  }

}
