import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebriefPageComponent } from './debrief-page.component';

describe('DebriefPageComponent', () => {
  let component: DebriefPageComponent;
  let fixture: ComponentFixture<DebriefPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebriefPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebriefPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
