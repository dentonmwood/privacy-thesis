import { Component, OnInit } from '@angular/core';
import {AppState} from "../../../app.state";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {User} from "../../../models/user";

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy-page.component.html',
  styleUrls: ['./privacy-policy-page.component.css']
})
export class PrivacyPolicyPageComponent implements OnInit {
  user: Observable<User>;
  trialNum: number;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.user = this.store.select('user');
    this.getTrialNum();
  }

  getTrialNum() {
    this.user.subscribe(user => {
      this.trialNum = user[1].trialNum;
    });
  }
}
