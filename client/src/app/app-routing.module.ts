import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PrivacyPolicyPageComponent} from "./components/pages/privacy-policy-page/privacy-policy-page.component";
import {ConsentPageComponent} from "./components/pages/consent-page/consent-page.component";
import {SignUpIntroPage} from "./components/pages/sign-up-intro-page/sign-up-intro-page.component";
import {DebriefPageComponent} from "./components/pages/debrief-page/debrief-page.component";
import {SignupPageComponent} from "./components/pages/signup-page/signup-page.component";
import {QuestionnairePageComponent} from "./components/pages/questionnaire-page/questionnaire-page.component";
import {ErrorRepeatUserComponent} from "./components/errors/error-repeat-user/error-repeat-user.component";
import {QuestionnaireIntroPageComponent} from "./components/pages/questionnaire-intro-page/questionnaire-intro-page.component";
import {ErrorBadAccessComponent} from "./components/errors/error-bad-access/error-bad-access.component";
import {AuthRedirectService} from "./services/auth-redirect.service";
import {WelcomePageComponent} from "./components/pages/welcome-page/welcome-page.component";

const routes: Routes = [
  { path: '', component: ConsentPageComponent},
  { path: 'welcome', component: WelcomePageComponent },
  { path: 'sign-up-intro', component: SignUpIntroPage, canActivate: [AuthRedirectService]},
  { path: 'sign-up', component: SignupPageComponent, canActivate: [AuthRedirectService] },
  { path: 'privacy-policy', component: PrivacyPolicyPageComponent, canActivate: [AuthRedirectService] },
  { path: 'questionnaire', component: QuestionnairePageComponent, canActivate: [AuthRedirectService] },
  { path: 'questionnaire-intro', component: QuestionnaireIntroPageComponent, canActivate: [AuthRedirectService] },
  { path: 'debrief', component: DebriefPageComponent, canActivate: [AuthRedirectService] },
  { path: 'error', children: [
      { path: 'user', component: ErrorRepeatUserComponent },
      { path: 'bad-access', component: ErrorBadAccessComponent }
    ]},
  //{ path: '**', component: ConsentPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
