import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { QuestionService } from "./services/question.service";
import { QuestionnaireFormComponent } from './components/forms/questionnaire-form/questionnaire-form.component';
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { QuestionnaireFormQuestionComponent } from './components/forms/questionnaire-form-question/questionnaire-form-question.component';
import { ConsentPageComponent } from './components/pages/consent-page/consent-page.component';
import { SignupFormComponent } from './components/forms/signup-form/signup-form.component';
import { PrivacyPolicyPageComponent } from './components/pages/privacy-policy-page/privacy-policy-page.component';
import { SignUpIntroPage } from './components/pages/sign-up-intro-page/sign-up-intro-page.component';
import { DebriefPageComponent } from './components/pages/debrief-page/debrief-page.component';
import { QuestionnairePageComponent } from './components/pages/questionnaire-page/questionnaire-page.component';
import { SignupPageComponent } from './components/pages/signup-page/signup-page.component';
import { TextPrivacyPolicyComponent } from './components/policies/text-privacy-policy/text-privacy-policy.component';
import { VideoPrivacyPolicyComponent } from './components/policies/video-privacy-policy/video-privacy-policy.component';
import { ErrorRepeatUserComponent } from './components/errors/error-repeat-user/error-repeat-user.component';
import { QuestionnaireIntroPageComponent } from './components/pages/questionnaire-intro-page/questionnaire-intro-page.component';
import {StoreModule} from "@ngrx/store";
import {reducer} from "./reducers/user.reducer";
import { ErrorBadAccessComponent } from './components/errors/error-bad-access/error-bad-access.component';
import { WelcomePageComponent } from './components/pages/welcome-page/welcome-page.component';
import {NgxYoutubePlayerModule} from "ngx-youtube-player";


@NgModule({
  declarations: [
    AppComponent,
    QuestionnaireFormComponent,
    QuestionnaireFormQuestionComponent,
    ConsentPageComponent,
    SignupFormComponent,
    PrivacyPolicyPageComponent,
    SignUpIntroPage,
    DebriefPageComponent,
    QuestionnairePageComponent,
    SignupPageComponent,
    TextPrivacyPolicyComponent,
    VideoPrivacyPolicyComponent,
    ErrorRepeatUserComponent,
    QuestionnaireIntroPageComponent,
    ErrorBadAccessComponent,
    WelcomePageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot({
      user: reducer
    }),
    NgxYoutubePlayerModule.forRoot(),
  ],
  providers: [QuestionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
