export class User {
  public constructor(
    public id?: number,
    public ipAddress?: string,
    public trialNum?: number,
    public answers?
  ) {}
}
