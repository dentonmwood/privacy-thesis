export class Answer {
  public constructor(
    public id: number,
    public text: string,
    public correct: boolean,
    public questionId: number
  ) {}
}
