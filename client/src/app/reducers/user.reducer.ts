import {User} from "../models/user";
import * as UserActions from "../actions/user.actions"

const initialState: User = {
  id: -1,
  ipAddress: '0.0.0.0',
  trialNum: -1,
  answers: null
};

export function reducer(state: User = initialState, action: UserActions.Actions) {
  switch(action.type) {
    case UserActions.ADD_USER:
      return [state, action.payload];
    default:
      return state;
  }
}
