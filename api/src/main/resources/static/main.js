(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/actions/user.actions.ts":
/*!*****************************************!*\
  !*** ./src/app/actions/user.actions.ts ***!
  \*****************************************/
/*! exports provided: ADD_USER, AddUser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_USER", function() { return ADD_USER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUser", function() { return AddUser; });
var ADD_USER = '[USER] Add';
var AddUser = /** @class */ (function () {
    function AddUser(payload) {
        this.payload = payload;
        this.type = ADD_USER;
    }
    return AddUser;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_pages_privacy_policy_page_privacy_policy_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/pages/privacy-policy-page/privacy-policy-page.component */ "./src/app/components/pages/privacy-policy-page/privacy-policy-page.component.ts");
/* harmony import */ var _components_pages_consent_page_consent_page_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/pages/consent-page/consent-page.component */ "./src/app/components/pages/consent-page/consent-page.component.ts");
/* harmony import */ var _components_pages_sign_up_intro_page_sign_up_intro_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/pages/sign-up-intro-page/sign-up-intro-page.component */ "./src/app/components/pages/sign-up-intro-page/sign-up-intro-page.component.ts");
/* harmony import */ var _components_pages_debrief_page_debrief_page_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/pages/debrief-page/debrief-page.component */ "./src/app/components/pages/debrief-page/debrief-page.component.ts");
/* harmony import */ var _components_pages_signup_page_signup_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/pages/signup-page/signup-page.component */ "./src/app/components/pages/signup-page/signup-page.component.ts");
/* harmony import */ var _components_pages_questionnaire_page_questionnaire_page_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/pages/questionnaire-page/questionnaire-page.component */ "./src/app/components/pages/questionnaire-page/questionnaire-page.component.ts");
/* harmony import */ var _components_errors_error_repeat_user_error_repeat_user_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/errors/error-repeat-user/error-repeat-user.component */ "./src/app/components/errors/error-repeat-user/error-repeat-user.component.ts");
/* harmony import */ var _components_pages_questionnaire_intro_page_questionnaire_intro_page_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/pages/questionnaire-intro-page/questionnaire-intro-page.component */ "./src/app/components/pages/questionnaire-intro-page/questionnaire-intro-page.component.ts");
/* harmony import */ var _components_errors_error_bad_access_error_bad_access_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/errors/error-bad-access/error-bad-access.component */ "./src/app/components/errors/error-bad-access/error-bad-access.component.ts");
/* harmony import */ var _services_auth_redirect_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/auth-redirect.service */ "./src/app/services/auth-redirect.service.ts");
/* harmony import */ var _components_pages_welcome_page_welcome_page_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/pages/welcome-page/welcome-page.component */ "./src/app/components/pages/welcome-page/welcome-page.component.ts");














var routes = [
    { path: '', component: _components_pages_consent_page_consent_page_component__WEBPACK_IMPORTED_MODULE_4__["ConsentPageComponent"] },
    { path: 'welcome', component: _components_pages_welcome_page_welcome_page_component__WEBPACK_IMPORTED_MODULE_13__["WelcomePageComponent"] },
    { path: 'sign-up-intro', component: _components_pages_sign_up_intro_page_sign_up_intro_page_component__WEBPACK_IMPORTED_MODULE_5__["SignUpIntroPage"], canActivate: [_services_auth_redirect_service__WEBPACK_IMPORTED_MODULE_12__["AuthRedirectService"]] },
    { path: 'sign-up', component: _components_pages_signup_page_signup_page_component__WEBPACK_IMPORTED_MODULE_7__["SignupPageComponent"], canActivate: [_services_auth_redirect_service__WEBPACK_IMPORTED_MODULE_12__["AuthRedirectService"]] },
    { path: 'privacy-policy', component: _components_pages_privacy_policy_page_privacy_policy_page_component__WEBPACK_IMPORTED_MODULE_3__["PrivacyPolicyPageComponent"], canActivate: [_services_auth_redirect_service__WEBPACK_IMPORTED_MODULE_12__["AuthRedirectService"]] },
    { path: 'questionnaire', component: _components_pages_questionnaire_page_questionnaire_page_component__WEBPACK_IMPORTED_MODULE_8__["QuestionnairePageComponent"], canActivate: [_services_auth_redirect_service__WEBPACK_IMPORTED_MODULE_12__["AuthRedirectService"]] },
    { path: 'questionnaire-intro', component: _components_pages_questionnaire_intro_page_questionnaire_intro_page_component__WEBPACK_IMPORTED_MODULE_10__["QuestionnaireIntroPageComponent"], canActivate: [_services_auth_redirect_service__WEBPACK_IMPORTED_MODULE_12__["AuthRedirectService"]] },
    { path: 'debrief', component: _components_pages_debrief_page_debrief_page_component__WEBPACK_IMPORTED_MODULE_6__["DebriefPageComponent"], canActivate: [_services_auth_redirect_service__WEBPACK_IMPORTED_MODULE_12__["AuthRedirectService"]] },
    { path: 'error', children: [
            { path: 'user', component: _components_errors_error_repeat_user_error_repeat_user_component__WEBPACK_IMPORTED_MODULE_9__["ErrorRepeatUserComponent"] },
            { path: 'bad-access', component: _components_errors_error_bad_access_error_bad_access_component__WEBPACK_IMPORTED_MODULE_11__["ErrorBadAccessComponent"] }
        ] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { scrollPositionRestoration: 'enabled' })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_question_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/question.service */ "./src/app/services/question.service.ts");



var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'client';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            providers: [_services_question_service__WEBPACK_IMPORTED_MODULE_2__["QuestionService"]],
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_question_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/question.service */ "./src/app/services/question.service.ts");
/* harmony import */ var _components_forms_questionnaire_form_questionnaire_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/forms/questionnaire-form/questionnaire-form.component */ "./src/app/components/forms/questionnaire-form/questionnaire-form.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _components_forms_questionnaire_form_question_questionnaire_form_question_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/forms/questionnaire-form-question/questionnaire-form-question.component */ "./src/app/components/forms/questionnaire-form-question/questionnaire-form-question.component.ts");
/* harmony import */ var _components_pages_consent_page_consent_page_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/pages/consent-page/consent-page.component */ "./src/app/components/pages/consent-page/consent-page.component.ts");
/* harmony import */ var _components_forms_signup_form_signup_form_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/forms/signup-form/signup-form.component */ "./src/app/components/forms/signup-form/signup-form.component.ts");
/* harmony import */ var _components_pages_privacy_policy_page_privacy_policy_page_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/pages/privacy-policy-page/privacy-policy-page.component */ "./src/app/components/pages/privacy-policy-page/privacy-policy-page.component.ts");
/* harmony import */ var _components_pages_sign_up_intro_page_sign_up_intro_page_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/pages/sign-up-intro-page/sign-up-intro-page.component */ "./src/app/components/pages/sign-up-intro-page/sign-up-intro-page.component.ts");
/* harmony import */ var _components_pages_debrief_page_debrief_page_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/pages/debrief-page/debrief-page.component */ "./src/app/components/pages/debrief-page/debrief-page.component.ts");
/* harmony import */ var _components_pages_questionnaire_page_questionnaire_page_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/pages/questionnaire-page/questionnaire-page.component */ "./src/app/components/pages/questionnaire-page/questionnaire-page.component.ts");
/* harmony import */ var _components_pages_signup_page_signup_page_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/pages/signup-page/signup-page.component */ "./src/app/components/pages/signup-page/signup-page.component.ts");
/* harmony import */ var _components_policies_text_privacy_policy_text_privacy_policy_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/policies/text-privacy-policy/text-privacy-policy.component */ "./src/app/components/policies/text-privacy-policy/text-privacy-policy.component.ts");
/* harmony import */ var _components_policies_video_privacy_policy_video_privacy_policy_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/policies/video-privacy-policy/video-privacy-policy.component */ "./src/app/components/policies/video-privacy-policy/video-privacy-policy.component.ts");
/* harmony import */ var _components_errors_error_repeat_user_error_repeat_user_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/errors/error-repeat-user/error-repeat-user.component */ "./src/app/components/errors/error-repeat-user/error-repeat-user.component.ts");
/* harmony import */ var _components_pages_questionnaire_intro_page_questionnaire_intro_page_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./components/pages/questionnaire-intro-page/questionnaire-intro-page.component */ "./src/app/components/pages/questionnaire-intro-page/questionnaire-intro-page.component.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _reducers_user_reducer__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./reducers/user.reducer */ "./src/app/reducers/user.reducer.ts");
/* harmony import */ var _components_errors_error_bad_access_error_bad_access_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/errors/error-bad-access/error-bad-access.component */ "./src/app/components/errors/error-bad-access/error-bad-access.component.ts");
/* harmony import */ var _components_pages_welcome_page_welcome_page_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/pages/welcome-page/welcome-page.component */ "./src/app/components/pages/welcome-page/welcome-page.component.ts");
/* harmony import */ var ngx_youtube_player__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ngx-youtube-player */ "./node_modules/ngx-youtube-player/fesm5/ngx-youtube-player.js");



























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _components_forms_questionnaire_form_questionnaire_form_component__WEBPACK_IMPORTED_MODULE_7__["QuestionnaireFormComponent"],
                _components_forms_questionnaire_form_question_questionnaire_form_question_component__WEBPACK_IMPORTED_MODULE_9__["QuestionnaireFormQuestionComponent"],
                _components_pages_consent_page_consent_page_component__WEBPACK_IMPORTED_MODULE_10__["ConsentPageComponent"],
                _components_forms_signup_form_signup_form_component__WEBPACK_IMPORTED_MODULE_11__["SignupFormComponent"],
                _components_pages_privacy_policy_page_privacy_policy_page_component__WEBPACK_IMPORTED_MODULE_12__["PrivacyPolicyPageComponent"],
                _components_pages_sign_up_intro_page_sign_up_intro_page_component__WEBPACK_IMPORTED_MODULE_13__["SignUpIntroPage"],
                _components_pages_debrief_page_debrief_page_component__WEBPACK_IMPORTED_MODULE_14__["DebriefPageComponent"],
                _components_pages_questionnaire_page_questionnaire_page_component__WEBPACK_IMPORTED_MODULE_15__["QuestionnairePageComponent"],
                _components_pages_signup_page_signup_page_component__WEBPACK_IMPORTED_MODULE_16__["SignupPageComponent"],
                _components_policies_text_privacy_policy_text_privacy_policy_component__WEBPACK_IMPORTED_MODULE_17__["TextPrivacyPolicyComponent"],
                _components_policies_video_privacy_policy_video_privacy_policy_component__WEBPACK_IMPORTED_MODULE_18__["VideoPrivacyPolicyComponent"],
                _components_errors_error_repeat_user_error_repeat_user_component__WEBPACK_IMPORTED_MODULE_19__["ErrorRepeatUserComponent"],
                _components_pages_questionnaire_intro_page_questionnaire_intro_page_component__WEBPACK_IMPORTED_MODULE_20__["QuestionnaireIntroPageComponent"],
                _components_errors_error_bad_access_error_bad_access_component__WEBPACK_IMPORTED_MODULE_23__["ErrorBadAccessComponent"],
                _components_pages_welcome_page_welcome_page_component__WEBPACK_IMPORTED_MODULE_24__["WelcomePageComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
                _ngrx_store__WEBPACK_IMPORTED_MODULE_21__["StoreModule"].forRoot({
                    user: _reducers_user_reducer__WEBPACK_IMPORTED_MODULE_22__["reducer"]
                }),
                ngx_youtube_player__WEBPACK_IMPORTED_MODULE_25__["NgxYoutubePlayerModule"].forRoot(),
            ],
            providers: [_services_question_service__WEBPACK_IMPORTED_MODULE_6__["QuestionService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/errors/error-bad-access/error-bad-access.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/components/errors/error-bad-access/error-bad-access.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZXJyb3JzL2Vycm9yLWJhZC1hY2Nlc3MvZXJyb3ItYmFkLWFjY2Vzcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/errors/error-bad-access/error-bad-access.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/components/errors/error-bad-access/error-bad-access.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"project\" class=\"page\">\n  <h1 class=\"heading\">Uh-oh!</h1>\n  <div class=\"page_body\">\n    <p>\n      Looks like you got here by accident! Make sure you don't refresh your browser when you're going through the experiment. Click the button below to go back to the consent form and try again.\n    </p>\n    <button class=\"button\" routerLink=\"/\">Go Back</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/errors/error-bad-access/error-bad-access.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/components/errors/error-bad-access/error-bad-access.component.ts ***!
  \**********************************************************************************/
/*! exports provided: ErrorBadAccessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorBadAccessComponent", function() { return ErrorBadAccessComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ErrorBadAccessComponent = /** @class */ (function () {
    function ErrorBadAccessComponent() {
    }
    ErrorBadAccessComponent.prototype.ngOnInit = function () {
    };
    ErrorBadAccessComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-error-bad-access',
            template: __webpack_require__(/*! ./error-bad-access.component.html */ "./src/app/components/errors/error-bad-access/error-bad-access.component.html"),
            styles: [__webpack_require__(/*! ./error-bad-access.component.css */ "./src/app/components/errors/error-bad-access/error-bad-access.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ErrorBadAccessComponent);
    return ErrorBadAccessComponent;
}());



/***/ }),

/***/ "./src/app/components/errors/error-repeat-user/error-repeat-user.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/components/errors/error-repeat-user/error-repeat-user.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZXJyb3JzL2Vycm9yLXJlcGVhdC11c2VyL2Vycm9yLXJlcGVhdC11c2VyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/errors/error-repeat-user/error-repeat-user.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/components/errors/error-repeat-user/error-repeat-user.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"project\" class=\"page\">\n  <h1 class=\"heading\">Uh-oh!</h1>\n  <div class=\"page_body\">\n    <p>\n      According to our records, it looks like you've already participated in this experiment! Although we appreciate your willingness to help, it would skew our data set to have you in there multiple times. If you believe you have received this message in error, please contact <a href=\"Denton_Wood@baylor.edu\">Denton Wood</a> to resolve this issue.\n    </p>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/errors/error-repeat-user/error-repeat-user.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/components/errors/error-repeat-user/error-repeat-user.component.ts ***!
  \************************************************************************************/
/*! exports provided: ErrorRepeatUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorRepeatUserComponent", function() { return ErrorRepeatUserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ErrorRepeatUserComponent = /** @class */ (function () {
    function ErrorRepeatUserComponent() {
    }
    ErrorRepeatUserComponent.prototype.ngOnInit = function () {
    };
    ErrorRepeatUserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-error-repeat-user',
            template: __webpack_require__(/*! ./error-repeat-user.component.html */ "./src/app/components/errors/error-repeat-user/error-repeat-user.component.html"),
            styles: [__webpack_require__(/*! ./error-repeat-user.component.css */ "./src/app/components/errors/error-repeat-user/error-repeat-user.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ErrorRepeatUserComponent);
    return ErrorRepeatUserComponent;
}());



/***/ }),

/***/ "./src/app/components/forms/questionnaire-form-question/questionnaire-form-question.component.css":
/*!********************************************************************************************************!*\
  !*** ./src/app/components/forms/questionnaire-form-question/questionnaire-form-question.component.css ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZm9ybXMvcXVlc3Rpb25uYWlyZS1mb3JtLXF1ZXN0aW9uL3F1ZXN0aW9ubmFpcmUtZm9ybS1xdWVzdGlvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/forms/questionnaire-form-question/questionnaire-form-question.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/components/forms/questionnaire-form-question/questionnaire-form-question.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [formGroup]=\"questionForm\">\n  <label [attr.for]=\"question.key\">{{(question.key) + \". \" + question.label}}</label>\n\n  <div [ngSwitch]=\"question.controlType\">\n\n    <input *ngSwitchCase=\"'textbox'\" [formControlName]=\"question.key\"\n           [id]=\"question.key\" [type]=\"question.type\">\n\n    <select [id]=\"question.key\" *ngSwitchCase=\"'dropdown'\" [formControlName]=\"question.key\">\n      <option *ngFor=\"let opt of question.options\" [value]=\"opt.key\">{{opt.value}}</option>\n    </select>\n\n    <div *ngSwitchCase=\"'radio'\">\n      <ng-container *ngFor=\"let opt of question.options\">\n        <label>\n          <input\n            type=\"radio\"\n            [name]=\"question.key\"\n            [formControlName]=\"question.key\"\n            [value]=\"opt.id\">\n          {{opt.text}}\n        </label>\n        <br/>\n      </ng-container>\n      <br/>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/forms/questionnaire-form-question/questionnaire-form-question.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/components/forms/questionnaire-form-question/questionnaire-form-question.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: QuestionnaireFormQuestionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionnaireFormQuestionComponent", function() { return QuestionnaireFormQuestionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_form_question_base__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../models/form/question-base */ "./src/app/models/form/question-base.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");




var QuestionnaireFormQuestionComponent = /** @class */ (function () {
    function QuestionnaireFormQuestionComponent() {
    }
    Object.defineProperty(QuestionnaireFormQuestionComponent.prototype, "isValid", {
        get: function () { return this.questionForm.controls[this.question.key].valid; },
        enumerable: true,
        configurable: true
    });
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_form_question_base__WEBPACK_IMPORTED_MODULE_2__["QuestionBase"])
    ], QuestionnaireFormQuestionComponent.prototype, "question", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"])
    ], QuestionnaireFormQuestionComponent.prototype, "questionForm", void 0);
    QuestionnaireFormQuestionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-questionnaire-form-question',
            template: __webpack_require__(/*! ./questionnaire-form-question.component.html */ "./src/app/components/forms/questionnaire-form-question/questionnaire-form-question.component.html"),
            styles: [__webpack_require__(/*! ./questionnaire-form-question.component.css */ "./src/app/components/forms/questionnaire-form-question/questionnaire-form-question.component.css")]
        })
    ], QuestionnaireFormQuestionComponent);
    return QuestionnaireFormQuestionComponent;
}());



/***/ }),

/***/ "./src/app/components/forms/questionnaire-form/questionnaire-form.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/components/forms/questionnaire-form/questionnaire-form.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZm9ybXMvcXVlc3Rpb25uYWlyZS1mb3JtL3F1ZXN0aW9ubmFpcmUtZm9ybS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/forms/questionnaire-form/questionnaire-form.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/components/forms/questionnaire-form/questionnaire-form.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"questions\">\n  <form [formGroup]=\"questionForm\">\n    <div *ngFor=\"let question of questions\" class=\"form-row\">\n      <app-questionnaire-form-question [question]=\"question\" [questionForm]=\"questionForm\"></app-questionnaire-form-question>\n    </div>\n    <div class=\"form-row\">\n      <button\n        class=\"button\"\n        type=\"submit\"\n        [disabled]=\"!questionForm.valid\"\n        routerLink=\"/debrief\"\n        (click)=\"onSubmit()\"\n      >Submit</button>\n    </div>\n  </form>\n</div>\n"

/***/ }),

/***/ "./src/app/components/forms/questionnaire-form/questionnaire-form.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/components/forms/questionnaire-form/questionnaire-form.component.ts ***!
  \*************************************************************************************/
/*! exports provided: QuestionnaireFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionnaireFormComponent", function() { return QuestionnaireFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_question_control_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/question-control.service */ "./src/app/services/question-control.service.ts");
/* harmony import */ var _services_question_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/question.service */ "./src/app/services/question.service.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _models_answer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../models/answer */ "./src/app/models/answer.ts");
/* harmony import */ var _models_form_radio_question__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../models/form/radio-question */ "./src/app/models/form/radio-question.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");








var QuestionnaireFormComponent = /** @class */ (function () {
    function QuestionnaireFormComponent(questionControlService, questionService, userService, store) {
        this.questionControlService = questionControlService;
        this.questionService = questionService;
        this.userService = userService;
        this.store = store;
    }
    QuestionnaireFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.questionService.getQuestions().subscribe(function (questions) {
            _this.questions = questions;
            _this.questionForm = _this.questionControlService.toFormGroup(questions);
            _this.user = _this.store.select('user');
        });
    };
    QuestionnaireFormComponent.prototype.onSubmit = function () {
        this.answers = [];
        var questionId;
        var i = 0;
        for (questionId in this.questionForm.value) {
            var answerId = this.questionForm.value[i + 1].id;
            if (this.questions[i] instanceof _models_form_radio_question__WEBPACK_IMPORTED_MODULE_6__["RadioQuestion"]) {
                var answer = this.questions[i].options[answerId];
                this.answers.push(new _models_answer__WEBPACK_IMPORTED_MODULE_5__["Answer"](answer.id.id, answer.text, answer.correct, answer.id.questionId));
            }
            i++;
        }
        this.saveAnswers();
    };
    QuestionnaireFormComponent.prototype.saveAnswers = function () {
        var _this = this;
        this.user.subscribe(function (user) {
            var userToPut = user[1];
            userToPut.answers = _this.answers;
            _this.userService.updateUser(userToPut);
        });
    };
    QuestionnaireFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-question-form',
            template: __webpack_require__(/*! ./questionnaire-form.component.html */ "./src/app/components/forms/questionnaire-form/questionnaire-form.component.html"),
            providers: [_services_question_control_service__WEBPACK_IMPORTED_MODULE_2__["QuestionControlService"]],
            styles: [__webpack_require__(/*! ./questionnaire-form.component.css */ "./src/app/components/forms/questionnaire-form/questionnaire-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_question_control_service__WEBPACK_IMPORTED_MODULE_2__["QuestionControlService"],
            _services_question_service__WEBPACK_IMPORTED_MODULE_3__["QuestionService"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_7__["Store"]])
    ], QuestionnaireFormComponent);
    return QuestionnaireFormComponent;
}());



/***/ }),

/***/ "./src/app/components/forms/signup-form/signup-form.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/forms/signup-form/signup-form.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZm9ybXMvc2lnbnVwLWZvcm0vc2lnbnVwLWZvcm0uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/forms/signup-form/signup-form.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/forms/signup-form/signup-form.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form>\n  <div class=\"form-group\">\n    <h3>Your Information</h3>\n    <div style=\"margin-left: 20px\">\n      <label>\n        First Name\n        <input type=\"text\" class=\"form-control\">\n      </label><br/>\n      <label>\n        Last Name\n        <input type=\"text\" class=\"form-control\">\n      </label><br/>\n      <label>\n        Email\n        <input type=\"text\" class=\"form-control\">\n      </label><br/>\n      <label>\n        Phone Number\n        <input type=\"text\" class=\"form-control\">\n      </label><br/>\n    </div>\n  </div>\n  <div class=\"form-group\">\n    <h3>Your Credentials</h3>\n    <div style=\"margin-left: 20px\">\n      <label>\n        Username\n        <input type=\"text\" class=\"form-control\">\n      </label><br/>\n      <label>\n        Password\n        <input type=\"password\" class=\"form-control\">\n      </label><br/>\n      <label>\n        Confirm Password\n        <input type=\"password\" class=\"form-control\">\n      </label><br/>\n    </div>\n  </div>\n  <div *ngIf=\"trialNum === 0; else elseBlock\">\n    <p>\n      By clicking \"Continue\", you indicate your agreement to have your data processed by our <a routerLink=\"/privacy-policy\">Privacy Policy</a>.\n    </p>\n    <button class=\"button\" routerLink=\"/questionnaire-intro\">Continue</button>\n  </div>\n  <ng-template #elseBlock>\n    <button class=\"button\" routerLink=\"/privacy-policy\">Continue</button>\n  </ng-template>\n</form>\n"

/***/ }),

/***/ "./src/app/components/forms/signup-form/signup-form.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/forms/signup-form/signup-form.component.ts ***!
  \***********************************************************************/
/*! exports provided: SignupFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupFormComponent", function() { return SignupFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");



var SignupFormComponent = /** @class */ (function () {
    function SignupFormComponent(store) {
        this.store = store;
    }
    SignupFormComponent.prototype.ngOnInit = function () {
        this.user = this.store.select('user');
        this.getTrialNum();
    };
    SignupFormComponent.prototype.getTrialNum = function () {
        var _this = this;
        this.user.subscribe(function (user) {
            _this.trialNum = user[1].trialNum;
        });
    };
    SignupFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup-form',
            template: __webpack_require__(/*! ./signup-form.component.html */ "./src/app/components/forms/signup-form/signup-form.component.html"),
            styles: [__webpack_require__(/*! ./signup-form.component.css */ "./src/app/components/forms/signup-form/signup-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]])
    ], SignupFormComponent);
    return SignupFormComponent;
}());



/***/ }),

/***/ "./src/app/components/pages/consent-page/consent-page.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/components/pages/consent-page/consent-page.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome {\n  text-align: center;\n}\n\n.divider {\n  height: 10px;\n  background: #e2c200;\n  margin-top: 30px;\n  margin-bottom: 50px;\n}\n\n.title {\n  text-align: center;\n}\n\n.subheading {\n  text-align: center;\n  background: forestgreen;\n  vertical-align: center;\n  padding: 15px;\n  color: white;\n}\n\n.text {\n  margin-left: 20px;\n  margin-right: 20px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wYWdlcy9jb25zZW50LXBhZ2UvY29uc2VudC1wYWdlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsdUJBQXVCO0VBQ3ZCLHNCQUFzQjtFQUN0QixhQUFhO0VBQ2IsWUFBWTtBQUNkOztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGtCQUFrQjtBQUNwQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGFnZXMvY29uc2VudC1wYWdlL2NvbnNlbnQtcGFnZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndlbGNvbWUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5kaXZpZGVyIHtcbiAgaGVpZ2h0OiAxMHB4O1xuICBiYWNrZ3JvdW5kOiAjZTJjMjAwO1xuICBtYXJnaW4tdG9wOiAzMHB4O1xuICBtYXJnaW4tYm90dG9tOiA1MHB4O1xufVxuXG4udGl0bGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5zdWJoZWFkaW5nIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kOiBmb3Jlc3RncmVlbjtcbiAgdmVydGljYWwtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZzogMTVweDtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4udGV4dCB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/components/pages/consent-page/consent-page.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/pages/consent-page/consent-page.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"project\" class=\"page\">\n  <div class=\"welcome\">\n    <h1>Welcome to my study!</h1>\n    <p>Please read the consent form below. If you agree to participate in the study, please select the box at the bottom of the page and click \"Begin Study.\"</p>\n  </div>\n  <div class=\"divider\"></div>\n  <div class=\"title\">\n    Baylor University<br />\n    <strong>Department of Computer Science</strong><br />\n    <h1>Consent Form for Research</h1><br/>\n  </div>\n  <div>\n    <p>PROTOCOL TITLE: <strong>Revamping the Privacy Policy: A Study on Informed Consent and User Interactions</strong><br/>\n      PRINCIPAL INVESTIGATOR: <strong>Denton Wood</strong></p>\n  </div>\n  <div>\n    <h2 class=\"subheading\">Invitation to be a part of a research study</h2>\n    <div class=\"text\">\n      <p>\n        You are invited to be part of a research study. This consent form will help you choose whether or not to participate in the study. Feel free to ask if anything is not clear in this consent form.\n      </p>\n    </div>\n  </div>\n  <div>\n    <h2 class=\"subheading\">Why is this study being done?</h2>\n    <div class=\"text\">\n      <p>\n        The purpose of this study is to understand how the format of privacy policies influences how well people understand them. In light of recent data privacy events such as the Facebook-Cambridge Analytica scandal and the launch of the European Union’s General Data Protection Regulation, we want to understand how companies can improve their privacy policies to help users understand what is happening with their data.\n      </p>\n    </div>\n  </div>\n  <div>\n    <h2 class=\"subheading\">What will happen if I take part in this research study?</h2>\n    <div class=\"text\">\n      <p>\n        If you agree to take part in this study, you will be asked to “sign up” for a faux social media service. There will be fields to supply personal information (such as name, address, email, etc.) None of this data will be collected or impact the study results. This process should take approximately 5-10 minutes. Afterwards, you will be given a brief questionnaire which should take approximately 5-10 minutes.\n      </p>\n      <p>\n        We will assign you by chance (like a coin toss) to one of four study groups. The difference in activities between the groups is intentionally being withheld to ensure that the study is valid. You and the researcher cannot choose your study group.  You will have an equal chance of being assigned to either study group.\n      </p>\n    </div>\n  </div>\n  <div>\n    <h2 class=\"subheading\">How long will I be in this study and how many people will be in the study?</h2>\n    <div class=\"text\">\n      <p>\n        Participation in this study will last approximately 20 minutes. About 400 subjects will take part in this research study.\n      </p>\n    </div>\n  </div>\n  <div>\n    <h2 class=\"subheading\">What are the risks of taking part in this research study?</h2>\n    <div class=\"text\">\n      <p>\n        Other than potential loss of confidentiality, as discussed below, there are no foreseeable risks to taking part in this study. However, as part of this research, you will not be told about some of the study details. If you were told these details at the beginning of the study, it could change the research results. If you decide to be part of the study, you will be given an explanation of what information was withheld from you at the end of your study participation.\n      </p>\n    </div>\n  </div>\n  <div>\n    <h2 class=\"subheading\">Are there any benefits from being in this research study?</h2>\n    <div class=\"text\">\n      <p>\n        Although you will not directly benefit from being in this study, others might benefit because the goal of the study is to encourage companies to change the way they present privacy policies in such a way as to increase user understanding.\n      </p>\n    </div>\n  </div>\n  <div>\n    <h2 class=\"subheading\">How Will You Protect my Information?</h2>\n    <div class=\"text\">\n      <p>\n        A risk of taking part in this study is the possibility of a loss of confidentiality. Loss of confidentiality includes having your personal information shared with someone who is not on the study team and was not supposed to see or know about your information. The researcher plans to protect your confidentiality.\n      </p>\n      <p>\n        We will keep the records of this study confidential by collecting no personally identifying information other than the IP address of your computer. We are doing this to ensure that people do not participate in the study twice, and this identifier will be deleted from the data once the study is complete (your data will then only be identified by a random number). The data that we do collect will be stored on password-protected computers and only be transferred using encrypted transfer. We will make every effort to keep your records confidential. However, there are times when federal or state law requires the disclosure of your records.\n      </p>\n      <p>\n        The following people or groups may review your study records for purposes such as quality control or safety:\n      </p>\n      <ul>\n        <li>Representatives of Baylor University and the BU Institutional Review Board</li>\n        <li>Federal and state agencies that oversee or review research (such as the HHS Office of Human Research Protection or the Food and Drug Administration)</li>\n      </ul>\n      <p>\n        The results of this study may also be used for teaching, publications, or presentations at professional meetings. If your individual results are discussed, your identity will be protected by using a code number or pseudonym rather than your name or other identifying information.\n      </p>\n    </div>\n  </div>\n  <div>\n    <h2 class=\"subheading\">Will I be compensated for being part of the study?</h2>\n    <div class=\"text\">\n      <p>\n        You will not be paid for taking part in this study.\n      </p>\n    </div>\n  </div>\n  <div>\n    <h2 class=\"subheading\">Your Participation in this Study is Voluntary</h2>\n    <div class=\"text\">\n      <p>\n        Taking part in this study is your choice.  You are free not to take part or to withdraw at any time for any reason.  No matter what you decide, there will be no penalty or loss of benefit to which you are entitled.  If you decide to withdraw from this study, the information that you have already provided will be kept confidential. You cannot withdraw information collected prior to your withdrawal.\n      </p>\n      <p>\n        If you are a Baylor student or faculty/staff member, you may choose not to be in the study or to stop being in the study before it is over at any time.  This will not affect your grades or job status at Baylor University.  You will not be offered or receive any special consideration if you take part in this research study.\n      </p>\n    </div>\n  </div>\n  <div>\n    <h2 class=\"subheading\">Contact Information for the Study Team and Questions about the Research</h2>\n    <div class=\"text\">\n      <p>\n        If you have any questions about this research, you may contact:\n      </p>\n      <p>\n        Denton Wood<br/>\n        Phone: (270) 508-0728<br/>\n        Email: <a href=\"mailto:Denton_Wood@baylor.edu\">Denton_Wood@baylor.edu</a><br/>\n      </p>\n      <p>Or</p>\n      <p>\n        Dr. Richard Sneed<br/>\n        Phone: (254) 710-4227<br/>\n        Email: <a href=\"mailto:Richard_Sneed@baylor.edu\">Richard_Sneed@baylor.edu</a><br/>\n      </p>\n    </div>\n  </div>\n  <div>\n    <h2 class=\"subheading\">Contact Information for Questions about Your Rights as a Research Participant</h2>\n    <div class=\"text\">\n      <p>\n        If you have questions about your rights as a research participant, or wish to obtain information, ask questions, or discuss any concerns about this study with someone other than the researcher(s), please contact the following:\n      </p>\n      <p>\n        Baylor University Institutional Review Board<br/>\n        Office of Research Compliance<br/>\n        Phone: 254-710-3708<br/>\n        Email: <a href=\"mailto:irb@baylor.edu\">irb@baylor.edu</a><br/>\n      </p>\n    </div>\n  </div>\n  <div>\n    <h2 class=\"subheading\">Your Consent</h2>\n    <div class=\"text\">\n      <h3>Signature of Subject</h3>\n      <p>\n        By checking the box below, you are agreeing to be in this study. You are free to print this document for your records. We will keep a copy with the study records.  If you have any questions about the study after you sign this document, you can contact the study team using the information provided above.\n      </p>\n      <form>\n        <input type=\"checkbox\" [checked]=\"consent\" (change)=\"consent = !consent\">\n        <label>&nbsp;I understand what the study is about and my questions so far have been answered. I agree to take part in this study.</label><br/><br/>\n        <button\n          class=\"button\"\n          [disabled]=\"!consent\"\n          routerLink=\"/welcome\"\n          (click)=\"onClick()\"\n        >Begin Study</button>\n      </form>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/pages/consent-page/consent-page.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/pages/consent-page/consent-page.component.ts ***!
  \*************************************************************************/
/*! exports provided: ConsentPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConsentPageComponent", function() { return ConsentPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _models_user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../models/user */ "./src/app/models/user.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _actions_user_actions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../actions/user.actions */ "./src/app/actions/user.actions.ts");






var ConsentPageComponent = /** @class */ (function () {
    function ConsentPageComponent(userService, store) {
        this.userService = userService;
        this.store = store;
    }
    ConsentPageComponent.prototype.ngOnInit = function () {
    };
    ConsentPageComponent.prototype.onClick = function () {
        var _this = this;
        this.userService.addUser(new _models_user__WEBPACK_IMPORTED_MODULE_3__["User"]())
            .subscribe(function (user) {
            _this.store.dispatch(new _actions_user_actions__WEBPACK_IMPORTED_MODULE_5__["AddUser"](user));
            _this.store.select('user').subscribe(function (user) {
                _this.userService.addUser(user);
            });
        });
    };
    ConsentPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-consent-form',
            template: __webpack_require__(/*! ./consent-page.component.html */ "./src/app/components/pages/consent-page/consent-page.component.html"),
            styles: [__webpack_require__(/*! ./consent-page.component.css */ "./src/app/components/pages/consent-page/consent-page.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"]])
    ], ConsentPageComponent);
    return ConsentPageComponent;
}());



/***/ }),

/***/ "./src/app/components/pages/debrief-page/debrief-page.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/components/pages/debrief-page/debrief-page.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGFnZXMvZGVicmllZi1wYWdlL2RlYnJpZWYtcGFnZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/pages/debrief-page/debrief-page.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/pages/debrief-page/debrief-page.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"project\" class=\"page\">\n  <h1 class=\"heading\">Debrief</h1>\n  <div class=\"page_body\">\n    <p>Thank you for participating in my study! This study is meant to research the effects of different ways of presenting the privacy policy on user understanding of the policy. There are two main factors I am studying:\n    </p>\n    <ol>\n      <li>Whether the policy is put in front of you, as opposed to being accessible through a link</li>\n      <li>The medium in which the policy was presented to you</li>\n    </ol>\n    <p>\n      When you entered the study, you were split into one of four groups. Groups 1 and 2 were testing the first factor. When members of Group 1 accessed the sign-up form, there was a link to view the privacy policy right above the \"Submit\" button. If you didn't click the link, however, you didn't see the policy. When Group 2 hit the submit button, the privacy policy was placed in front of them, and they had the opportunity to read it.\n    </p>\n    <p>\n      Groups 3 and 4 were testing the second factor. Group 3's privacy policy was your average text-based privacy policy. Group 4, however, got access to a video policy that explained Flipper's policies with whiteboard drawings.\n    </p>\n    <p>You were in Group {{trialNum + 1}}. If you missed the video policy and would like to view it, you can find it <a href=\"https://youtu.be/74tQEjTUfSo\" target=\"_blank\">here.</a></p>\n    <p>\n      All four groups received the same questionnaire afterward. The questionnaire is being used as a measure of how well you understood the policy. As I review the results, I will be analyzing which group in each pair got the most questions right on the questionnaire. My hypothesis is that Groups 2 and 4 will have a greater understanding of the privacy policy than Groups 1 and 3.\n    </p>\n    <p>\n      Thank you again for helping me with my research. If you have any questions about my methods, how I'm storing the experiment data, etc., you are certainly welcome to contact me at <a href=\"mailto:Denton_Wood@baylor.edu\">Denton_Wood@baylor.edu</a>. Otherwise, you are done with the study, and I wish you a wonderful rest of your day!\n    </p>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/pages/debrief-page/debrief-page.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/pages/debrief-page/debrief-page.component.ts ***!
  \*************************************************************************/
/*! exports provided: DebriefPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DebriefPageComponent", function() { return DebriefPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");



var DebriefPageComponent = /** @class */ (function () {
    function DebriefPageComponent(store) {
        this.store = store;
    }
    DebriefPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.store.select('user').subscribe(function (user) {
            _this.trialNum = user[1].trialNum;
        });
    };
    DebriefPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-debrief',
            template: __webpack_require__(/*! ./debrief-page.component.html */ "./src/app/components/pages/debrief-page/debrief-page.component.html"),
            styles: [__webpack_require__(/*! ./debrief-page.component.css */ "./src/app/components/pages/debrief-page/debrief-page.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]])
    ], DebriefPageComponent);
    return DebriefPageComponent;
}());



/***/ }),

/***/ "./src/app/components/pages/privacy-policy-page/privacy-policy-page.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/components/pages/privacy-policy-page/privacy-policy-page.component.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGFnZXMvcHJpdmFjeS1wb2xpY3ktcGFnZS9wcml2YWN5LXBvbGljeS1wYWdlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/pages/privacy-policy-page/privacy-policy-page.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/components/pages/privacy-policy-page/privacy-policy-page.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"project\" class=\"page\">\n  <h1 class=\"heading\">Privacy Policy</h1>\n  <div class=\"page_body\">\n    <div *ngIf=\"trialNum === 3; else elseBlock\">\n      <app-video-privacy-policy></app-video-privacy-policy>\n    </div>\n    <ng-template #elseBlock>\n      <app-text-privacy-policy></app-text-privacy-policy>\n    </ng-template>\n    <br/>\n    <button class=\"button\" routerLink=\"/questionnaire-intro\">Continue</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/pages/privacy-policy-page/privacy-policy-page.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/components/pages/privacy-policy-page/privacy-policy-page.component.ts ***!
  \***************************************************************************************/
/*! exports provided: PrivacyPolicyPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivacyPolicyPageComponent", function() { return PrivacyPolicyPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");



var PrivacyPolicyPageComponent = /** @class */ (function () {
    function PrivacyPolicyPageComponent(store) {
        this.store = store;
    }
    PrivacyPolicyPageComponent.prototype.ngOnInit = function () {
        this.user = this.store.select('user');
        this.getTrialNum();
    };
    PrivacyPolicyPageComponent.prototype.getTrialNum = function () {
        var _this = this;
        this.user.subscribe(function (user) {
            _this.trialNum = user[1].trialNum;
        });
    };
    PrivacyPolicyPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-privacy-policy',
            template: __webpack_require__(/*! ./privacy-policy-page.component.html */ "./src/app/components/pages/privacy-policy-page/privacy-policy-page.component.html"),
            styles: [__webpack_require__(/*! ./privacy-policy-page.component.css */ "./src/app/components/pages/privacy-policy-page/privacy-policy-page.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]])
    ], PrivacyPolicyPageComponent);
    return PrivacyPolicyPageComponent;
}());



/***/ }),

/***/ "./src/app/components/pages/questionnaire-intro-page/questionnaire-intro-page.component.css":
/*!**************************************************************************************************!*\
  !*** ./src/app/components/pages/questionnaire-intro-page/questionnaire-intro-page.component.css ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGFnZXMvcXVlc3Rpb25uYWlyZS1pbnRyby1wYWdlL3F1ZXN0aW9ubmFpcmUtaW50cm8tcGFnZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/pages/questionnaire-intro-page/questionnaire-intro-page.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/components/pages/questionnaire-intro-page/questionnaire-intro-page.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"project\" class=\"page\">\n  <h1 class=\"heading\">Part 2: Questionnaire</h1>\n  <div class=\"page_body\">\n    <p>Thank you for participating in the simulation! You will now fill out a brief questionnaire regarding Flipper's privacy policy. Please do not use your browser's back button to check your work - just answer as best you can remember (even if you didn't read the policy). Press the button below to continue.</p>\n    <button class=\"button\" routerLink=\"/questionnaire\">Continue</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/pages/questionnaire-intro-page/questionnaire-intro-page.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/pages/questionnaire-intro-page/questionnaire-intro-page.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: QuestionnaireIntroPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionnaireIntroPageComponent", function() { return QuestionnaireIntroPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var QuestionnaireIntroPageComponent = /** @class */ (function () {
    function QuestionnaireIntroPageComponent() {
    }
    QuestionnaireIntroPageComponent.prototype.ngOnInit = function () {
    };
    QuestionnaireIntroPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-questionnaire-intro-page',
            template: __webpack_require__(/*! ./questionnaire-intro-page.component.html */ "./src/app/components/pages/questionnaire-intro-page/questionnaire-intro-page.component.html"),
            styles: [__webpack_require__(/*! ./questionnaire-intro-page.component.css */ "./src/app/components/pages/questionnaire-intro-page/questionnaire-intro-page.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], QuestionnaireIntroPageComponent);
    return QuestionnaireIntroPageComponent;
}());



/***/ }),

/***/ "./src/app/components/pages/questionnaire-page/questionnaire-page.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/components/pages/questionnaire-page/questionnaire-page.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGFnZXMvcXVlc3Rpb25uYWlyZS1wYWdlL3F1ZXN0aW9ubmFpcmUtcGFnZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/pages/questionnaire-page/questionnaire-page.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/components/pages/questionnaire-page/questionnaire-page.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"project\" class=\"page\">\n  <h1 class=\"heading\">Questionnaire</h1>\n  <div class=\"page_body\">\n    <p>\n      Please answer the following questions regarding Flipper's privacy policy. When you are finished, click the \"Submit\" button at the bottom of the page. The questionnaire may take a second to load.\n    </p>\n    <br />\n    <app-question-form></app-question-form>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/pages/questionnaire-page/questionnaire-page.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/components/pages/questionnaire-page/questionnaire-page.component.ts ***!
  \*************************************************************************************/
/*! exports provided: QuestionnairePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionnairePageComponent", function() { return QuestionnairePageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var QuestionnairePageComponent = /** @class */ (function () {
    function QuestionnairePageComponent() {
    }
    QuestionnairePageComponent.prototype.ngOnInit = function () {
    };
    QuestionnairePageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-questionnaire-page',
            template: __webpack_require__(/*! ./questionnaire-page.component.html */ "./src/app/components/pages/questionnaire-page/questionnaire-page.component.html"),
            styles: [__webpack_require__(/*! ./questionnaire-page.component.css */ "./src/app/components/pages/questionnaire-page/questionnaire-page.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], QuestionnairePageComponent);
    return QuestionnairePageComponent;
}());



/***/ }),

/***/ "./src/app/components/pages/sign-up-intro-page/sign-up-intro-page.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/components/pages/sign-up-intro-page/sign-up-intro-page.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGFnZXMvc2lnbi11cC1pbnRyby1wYWdlL3NpZ24tdXAtaW50cm8tcGFnZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/pages/sign-up-intro-page/sign-up-intro-page.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/components/pages/sign-up-intro-page/sign-up-intro-page.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"project\" class=\"page\">\n  <h1 class=\"heading\">Part 1: Simulation</h1>\n  <div class=\"divider\"></div>\n  <div class=\"page_body\">\n    <p>Please read the following scenario and do your best to act like it is happening to you.</p>\n    <p style=\"margin-left: 30px\"><em>\n      Your friends have been raving about a new social media site called \"Flipper\" and are insisting you sign up for an account right now. Flipper, it turns out, is a site designed to help people with common interests meet. Users list their interests and post pictures, videos, and text related to those interests. Users also maintain a list of \"friends,\" and Flipper scans the friends of a user's friends to find matching interests. If a match is found, users are encouraged to exchange, or \"flip,\" friends for a short time. The example given by Flipper's home page is two friends, Gennie and Clay. Gennie enjoys rock-climbing, and so does Clay's friend Peter. Clay, however, enjoys running, and so does Gennie's friend Rachel. Flipper asks Gennie and Clay to flip friends for a bit - Gennie gets to know Peter, and Clay gets to know Rachel. If either pair enjoys the conversation, they can opt to become friends. You are interested, and you decide to try the service.\n    </em></p>\n    <p>\n      On the next 1-2 pages, you will \"sign up\" for Flipper.\n    </p>\n    <p><strong>No data you enter in the sign-up process will be stored</strong>. The only piece of identifying information I will have for you is your IP address, and that will  be removed from the data when the study is complete. Please feel free to enter whatever you like in these fields - their sole purpose is to simulate a sign-up process for a real social media service.\n    </p>\n    <button class=\"button\" routerLink=\"/sign-up\">Continue</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/pages/sign-up-intro-page/sign-up-intro-page.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/components/pages/sign-up-intro-page/sign-up-intro-page.component.ts ***!
  \*************************************************************************************/
/*! exports provided: SignUpIntroPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpIntroPage", function() { return SignUpIntroPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SignUpIntroPage = /** @class */ (function () {
    function SignUpIntroPage() {
    }
    SignUpIntroPage.prototype.ngOnInit = function () {
    };
    SignUpIntroPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-introduction',
            template: __webpack_require__(/*! ./sign-up-intro-page.component.html */ "./src/app/components/pages/sign-up-intro-page/sign-up-intro-page.component.html"),
            styles: [__webpack_require__(/*! ./sign-up-intro-page.component.css */ "./src/app/components/pages/sign-up-intro-page/sign-up-intro-page.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SignUpIntroPage);
    return SignUpIntroPage;
}());



/***/ }),

/***/ "./src/app/components/pages/signup-page/signup-page.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/pages/signup-page/signup-page.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGFnZXMvc2lnbnVwLXBhZ2Uvc2lnbnVwLXBhZ2UuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/pages/signup-page/signup-page.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/pages/signup-page/signup-page.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"project\" class=\"page\">\n  <h1 class=\"heading\">Sign Up for Flipper!</h1>\n  <div class=\"page_body\">\n    <p style=\"margin-bottom: 25px\">Welcome to Flipper! We just need a few pieces of information from you, and you're ready to start flipping!</p>\n    <app-signup-form></app-signup-form>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/pages/signup-page/signup-page.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/pages/signup-page/signup-page.component.ts ***!
  \***********************************************************************/
/*! exports provided: SignupPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageComponent", function() { return SignupPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SignupPageComponent = /** @class */ (function () {
    function SignupPageComponent() {
    }
    SignupPageComponent.prototype.ngOnInit = function () {
    };
    SignupPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup-page',
            template: __webpack_require__(/*! ./signup-page.component.html */ "./src/app/components/pages/signup-page/signup-page.component.html"),
            styles: [__webpack_require__(/*! ./signup-page.component.css */ "./src/app/components/pages/signup-page/signup-page.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SignupPageComponent);
    return SignupPageComponent;
}());



/***/ }),

/***/ "./src/app/components/pages/welcome-page/welcome-page.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/components/pages/welcome-page/welcome-page.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGFnZXMvd2VsY29tZS1wYWdlL3dlbGNvbWUtcGFnZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/pages/welcome-page/welcome-page.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/pages/welcome-page/welcome-page.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"project\" class=\"page\">\n  <h1 class=\"heading\">Welcome!</h1>\n  <div class=\"page_body\">\n    <p>\n      This study consists of two parts. Click the button below to begin Part 1. Please do not use your browser's \"Back\" or \"Refresh\" buttons during the study, as they could interfere with the study interface.\n    </p>\n    <button class=\"button\" routerLink=\"/sign-up-intro\">Continue</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/pages/welcome-page/welcome-page.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/pages/welcome-page/welcome-page.component.ts ***!
  \*************************************************************************/
/*! exports provided: WelcomePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomePageComponent", function() { return WelcomePageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WelcomePageComponent = /** @class */ (function () {
    function WelcomePageComponent() {
    }
    WelcomePageComponent.prototype.ngOnInit = function () {
    };
    WelcomePageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-welcome',
            template: __webpack_require__(/*! ./welcome-page.component.html */ "./src/app/components/pages/welcome-page/welcome-page.component.html"),
            styles: [__webpack_require__(/*! ./welcome-page.component.css */ "./src/app/components/pages/welcome-page/welcome-page.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WelcomePageComponent);
    return WelcomePageComponent;
}());



/***/ }),

/***/ "./src/app/components/policies/text-privacy-policy/text-privacy-policy.component.css":
/*!*******************************************************************************************!*\
  !*** ./src/app/components/policies/text-privacy-policy/text-privacy-policy.component.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcG9saWNpZXMvdGV4dC1wcml2YWN5LXBvbGljeS90ZXh0LXByaXZhY3ktcG9saWN5LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/policies/text-privacy-policy/text-privacy-policy.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/components/policies/text-privacy-policy/text-privacy-policy.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <p>Effective date: April 12, 2019</p>\n\n\n  <p>Flipper, Inc. (\"us\", \"we\", or \"our\") operates the https://flipper.com website (hereinafter referred to as the \"Service\").</p>\n\n  <p>This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data. Our Privacy Policy  for Flipper, Inc. is created with the help of the <a href=\"https://privacypolicies.com/privacy-policy-generator/\">PrivacyPolicies.com Privacy Policy Generator</a>.</p>\n\n  <p>We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, the terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, accessible from https://flipper.com</p>\n\n\n  <h2>Information Collection And Use</h2>\n\n  <p>We collect several different types of information for various purposes to provide and improve our Service to you.</p>\n\n  <h3>Types of Data Collected</h3>\n\n  <h4>Personal Data</h4>\n\n  <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you (\"Personal Data\"). Personally identifiable information may include, but is not limited to:</p>\n\n  <ul>\n    <li>Email address</li><li>First name and last name</li><li>Phone number</li><li>Cookies and Usage Data</li>\n  </ul>\n\n  <h4>Usage Data</h4>\n\n  <p>We may also collect information on how the Service is accessed and used (\"Usage Data\"). This Usage Data may include information such as your computer's Internet Protocol address (e.g. IP address), browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.</p>\n\n  <h4>Tracking & Cookies Data</h4>\n  <p>We use cookies and similar tracking technologies to track the activity on our Service and hold certain information.</p>\n  <p>Cookies are files with small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and scripts to collect and track information and to improve and analyze our Service.</p>\n  <p>You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service. You can learn more how to manage cookies in the <a href=\"https://privacypolicies.com/blog/how-to-delete-cookies/\">Browser Cookies Guide</a>.</p>\n  <p>Examples of Cookies we use:</p>\n  <ul>\n    <li><strong>Session Cookies.</strong> We use Session Cookies to operate our Service.</li>\n    <li><strong>Preference Cookies.</strong> We use Preference Cookies to remember your preferences and various settings.</li>\n    <li><strong>Security Cookies.</strong> We use Security Cookies for security purposes.</li>\n  </ul>\n\n  <h2>Use of Data</h2>\n\n  <p>Flipper, Inc. uses the collected data for various purposes:</p>\n  <ul>\n    <li>To provide and maintain the Service</li>\n    <li>To notify you about changes to our Service</li>\n    <li>To allow you to participate in interactive features of our Service when you choose to do so</li>\n    <li>To provide customer care and support</li>\n    <li>To provide analysis or valuable information so that we can improve the Service</li>\n    <li>To monitor the usage of the Service</li>\n    <li>To detect, prevent and address technical issues</li>\n  </ul>\n\n  <h2>Transfer Of Data</h2>\n  <p>Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>\n  <p>If you are located outside United States and choose to provide information to us, please note that we transfer the data, including Personal Data, to United States and process it there.</p>\n  <p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>\n  <p>Flipper, Inc. will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.</p>\n\n  <h2>Disclosure Of Data</h2>\n\n  <h3>Legal Requirements</h3>\n  <p>Flipper, Inc. may disclose your Personal Data in the good faith belief that such action is necessary to:</p>\n  <ul>\n    <li>To comply with a legal obligation</li>\n    <li>To protect and defend the rights or property of Flipper, Inc.</li>\n    <li>To prevent or investigate possible wrongdoing in connection with the Service</li>\n    <li>To protect the personal safety of users of the Service or the public</li>\n    <li>To protect against legal liability</li>\n  </ul>\n\n  <h2>Security Of Data</h2>\n  <p>The security of your data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.</p>\n\n  <h2>Service Providers</h2>\n  <p>We may employ third party companies and individuals to facilitate our Service (\"Service Providers\"), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p>\n  <p>These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p>\n\n  <h3>Analytics</h3>\n  <p>We may use third-party Service Providers to monitor and analyze the use of our Service.</p>\n  <ul>\n    <li>\n      <p><strong>Google Analytics</strong></p>\n      <p>Google Analytics is a web analytics service offered by Google that tracks and reports website traffic. Google uses the data collected to track and monitor the use of our Service. This data is shared with other Google services. Google may use the collected data to contextualize and personalize the ads of its own advertising network.</p>\n      <p>You can opt-out of having made your activity on the Service available to Google Analytics by installing the Google Analytics opt-out browser add-on. The add-on prevents the Google Analytics JavaScript (ga.js, analytics.js, and dc.js) from sharing information with Google Analytics about visits activity.</p>                <p>For more information on the privacy practices of Google, please visit the Google Privacy & Terms web page: <a href=\"https://policies.google.com/privacy?hl=en\">https://policies.google.com/privacy?hl=en</a></p>\n    </li>\n  </ul>\n\n\n  <h2>Links To Other Sites</h2>\n  <p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>\n  <p>We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>\n\n\n  <h2>Children's Privacy</h2>\n  <p>Our Service does not address anyone under the age of 18 (\"Children\").</p>\n  <p>We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent or guardian and you are aware that your Children has provided us with Personal Data, please contact us. If we become aware that we have collected Personal Data from children without verification of parental consent, we take steps to remove that information from our servers.</p>\n\n\n  <h2>Changes To This Privacy Policy</h2>\n  <p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.</p>\n  <p>We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the \"effective date\" at the top of this Privacy Policy.</p>\n  <p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>\n\n\n  <h2>Contact Us</h2>\n  <p>If you have any questions about this Privacy Policy, please contact us:</p>\n  <ul>\n    <li>By email: info@flipper.com</li>\n\n  </ul>\n  <br/>\n  <p>By clicking the button below, I give my consent to my data being processed in accordance with this privacy policy.</p>\n</div>\n"

/***/ }),

/***/ "./src/app/components/policies/text-privacy-policy/text-privacy-policy.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/components/policies/text-privacy-policy/text-privacy-policy.component.ts ***!
  \******************************************************************************************/
/*! exports provided: TextPrivacyPolicyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextPrivacyPolicyComponent", function() { return TextPrivacyPolicyComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TextPrivacyPolicyComponent = /** @class */ (function () {
    function TextPrivacyPolicyComponent() {
    }
    TextPrivacyPolicyComponent.prototype.ngOnInit = function () {
    };
    TextPrivacyPolicyComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-text-privacy-policy',
            template: __webpack_require__(/*! ./text-privacy-policy.component.html */ "./src/app/components/policies/text-privacy-policy/text-privacy-policy.component.html"),
            styles: [__webpack_require__(/*! ./text-privacy-policy.component.css */ "./src/app/components/policies/text-privacy-policy/text-privacy-policy.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TextPrivacyPolicyComponent);
    return TextPrivacyPolicyComponent;
}());



/***/ }),

/***/ "./src/app/components/policies/video-privacy-policy/video-privacy-policy.component.css":
/*!*********************************************************************************************!*\
  !*** ./src/app/components/policies/video-privacy-policy/video-privacy-policy.component.css ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcG9saWNpZXMvdmlkZW8tcHJpdmFjeS1wb2xpY3kvdmlkZW8tcHJpdmFjeS1wb2xpY3kuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/policies/video-privacy-policy/video-privacy-policy.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/components/policies/video-privacy-policy/video-privacy-policy.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <p>You may view our privacy policy by watching the video below.</p>\n  <youtube-player\n    [videoId]=\"id\"\n    (ready)=\"savePlayer($event)\"\n    (change)=\"onStateChange($event)\"\n  ></youtube-player>\n</div>\n"

/***/ }),

/***/ "./src/app/components/policies/video-privacy-policy/video-privacy-policy.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/components/policies/video-privacy-policy/video-privacy-policy.component.ts ***!
  \********************************************************************************************/
/*! exports provided: videoURL, VideoPrivacyPolicyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "videoURL", function() { return videoURL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoPrivacyPolicyComponent", function() { return VideoPrivacyPolicyComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var videoURL = 'https://youtu.be/74tQEjTUfSo';
var VideoPrivacyPolicyComponent = /** @class */ (function () {
    function VideoPrivacyPolicyComponent() {
        this.id = '74tQEjTUfSo';
    }
    VideoPrivacyPolicyComponent.prototype.savePlayer = function (player) {
        this.player = player;
    };
    VideoPrivacyPolicyComponent.prototype.onStateChange = function (event) {
        console.log('player state', event.data);
    };
    VideoPrivacyPolicyComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-video-privacy-policy',
            template: __webpack_require__(/*! ./video-privacy-policy.component.html */ "./src/app/components/policies/video-privacy-policy/video-privacy-policy.component.html"),
            styles: [__webpack_require__(/*! ./video-privacy-policy.component.css */ "./src/app/components/policies/video-privacy-policy/video-privacy-policy.component.css")]
        })
    ], VideoPrivacyPolicyComponent);
    return VideoPrivacyPolicyComponent;
}());



/***/ }),

/***/ "./src/app/models/answer.ts":
/*!**********************************!*\
  !*** ./src/app/models/answer.ts ***!
  \**********************************/
/*! exports provided: Answer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Answer", function() { return Answer; });
var Answer = /** @class */ (function () {
    function Answer(id, text, correct, questionId) {
        this.id = id;
        this.text = text;
        this.correct = correct;
        this.questionId = questionId;
    }
    return Answer;
}());



/***/ }),

/***/ "./src/app/models/form/question-base.ts":
/*!**********************************************!*\
  !*** ./src/app/models/form/question-base.ts ***!
  \**********************************************/
/*! exports provided: QuestionBase */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionBase", function() { return QuestionBase; });
var QuestionBase = /** @class */ (function () {
    function QuestionBase(options) {
        if (options === void 0) { options = {}; }
        this.value = options.value;
        this.key = options.key || '';
        this.label = options.label || '';
        this.required = !!options.required;
        this.order = options.order === undefined ? 1 : options.order;
        this.controlType = options.controlType || '';
    }
    return QuestionBase;
}());



/***/ }),

/***/ "./src/app/models/form/radio-question.ts":
/*!***********************************************!*\
  !*** ./src/app/models/form/radio-question.ts ***!
  \***********************************************/
/*! exports provided: RadioQuestion */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RadioQuestion", function() { return RadioQuestion; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _question_base__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./question-base */ "./src/app/models/form/question-base.ts");


var RadioQuestion = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](RadioQuestion, _super);
    function RadioQuestion(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.controlType = 'radio';
        _this.options = [];
        _this.options = options['options'] || [];
        return _this;
    }
    return RadioQuestion;
}(_question_base__WEBPACK_IMPORTED_MODULE_1__["QuestionBase"]));



/***/ }),

/***/ "./src/app/models/user.ts":
/*!********************************!*\
  !*** ./src/app/models/user.ts ***!
  \********************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User(id, ipAddress, trialNum, answers) {
        this.id = id;
        this.ipAddress = ipAddress;
        this.trialNum = trialNum;
        this.answers = answers;
    }
    return User;
}());



/***/ }),

/***/ "./src/app/reducers/user.reducer.ts":
/*!******************************************!*\
  !*** ./src/app/reducers/user.reducer.ts ***!
  \******************************************/
/*! exports provided: reducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reducer", function() { return reducer; });
/* harmony import */ var _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/user.actions */ "./src/app/actions/user.actions.ts");

var initialState = {
    id: -1,
    ipAddress: '0.0.0.0',
    trialNum: -1,
    answers: null
};
function reducer(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["ADD_USER"]:
            return [state, action.payload];
        default:
            return state;
    }
}


/***/ }),

/***/ "./src/app/services/auth-redirect.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/auth-redirect.service.ts ***!
  \***************************************************/
/*! exports provided: AuthRedirectService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthRedirectService", function() { return AuthRedirectService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");




var AuthRedirectService = /** @class */ (function () {
    function AuthRedirectService(router, store) {
        this.router = router;
        this.store = store;
    }
    AuthRedirectService.prototype.canActivate = function (route, state) {
        var _this = this;
        var retval = true;
        this.store.select('user').subscribe(function (user) {
            if (user.id == -1) {
                _this.router.navigate(["/error/bad-access"]);
                retval = false;
            }
        });
        return retval;
    };
    AuthRedirectService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]])
    ], AuthRedirectService);
    return AuthRedirectService;
}());



/***/ }),

/***/ "./src/app/services/question-control.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/question-control.service.ts ***!
  \******************************************************/
/*! exports provided: QuestionControlService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionControlService", function() { return QuestionControlService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");



var QuestionControlService = /** @class */ (function () {
    function QuestionControlService() {
    }
    QuestionControlService.prototype.toFormGroup = function (questions) {
        var group = {};
        questions.forEach(function (question) {
            group[question.key] = question.required ? new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](question.value || '', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
                : new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](question.value || '');
        });
        return new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"](group);
    };
    QuestionControlService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], QuestionControlService);
    return QuestionControlService;
}());



/***/ }),

/***/ "./src/app/services/question.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/question.service.ts ***!
  \**********************************************/
/*! exports provided: QuestionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionService", function() { return QuestionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _models_form_radio_question__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/form/radio-question */ "./src/app/models/form/radio-question.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs_internal_observable_of__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/internal/observable/of */ "./node_modules/rxjs/internal/observable/of.js");
/* harmony import */ var rxjs_internal_observable_of__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_observable_of__WEBPACK_IMPORTED_MODULE_5__);






var QuestionService = /** @class */ (function () {
    function QuestionService(http) {
        this.http = http;
        this.questionsUrl = '//and.ecs.baylor.edu:8080/questions';
    }
    QuestionService.prototype.getQuestions = function () {
        var _this = this;
        return this.http.get(this.questionsUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (res) { return res.map(function (question) { return new _models_form_radio_question__WEBPACK_IMPORTED_MODULE_3__["RadioQuestion"]({
            key: question.id + 1,
            label: question.text,
            type: 'radio',
            options: _this.getAnswers(question),
            required: true,
            order: 1
        }); }); }, Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError('getServerQuestions'))));
    };
    QuestionService.prototype.getAnswers = function (question) {
        var answer;
        var answers = [];
        for (var _i = 0, _a = question.answers; _i < _a.length; _i++) {
            answer = _a[_i];
            answers.push({
                id: answer.id,
                text: answer.text,
                correct: answer.correct
            });
        }
        return answers;
    };
    QuestionService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            console.error(error);
            return Object(rxjs_internal_observable_of__WEBPACK_IMPORTED_MODULE_5__["of"])(result);
        };
    };
    QuestionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], QuestionService);
    return QuestionService;
}());



/***/ }),

/***/ "./src/app/services/user.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.usersUrl = '//and.ecs.baylor.edu:8080/users';
    }
    UserService.prototype.addUser = function (user) {
        return this.http.post(this.usersUrl, user, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError('addUser')));
    };
    UserService.prototype.updateUser = function (user) {
        var url = this.usersUrl + "/" + user.id;
        var payload = this.buildPostRequest(user);
        var response = this.http.post(url, payload, httpOptions);
        response.subscribe(function (user) { return console.log("Thanks!"); });
        return response;
    };
    UserService.prototype.buildPostRequest = function (user) {
        var answers = [];
        for (var answer in user.answers) {
            answers.push(user.answers[answer].id);
        }
        return { id: user.id, answers: answers };
    };
    UserService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            console.error(error);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(result);
        };
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/denton/Coding/thesis/client/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map