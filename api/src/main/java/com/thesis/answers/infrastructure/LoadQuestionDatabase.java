package com.thesis.answers.infrastructure;

import com.thesis.answers.domain.Answer;
import com.thesis.answers.domain.Question;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
@Slf4j
public class LoadQuestionDatabase {

    public List<Question> addQuestionsAndAnswers() {

        List<Question> questions = new ArrayList<>();
        List<List<Answer>> answerList = new ArrayList<>();

        Question question = new Question(0, "Flipper uses cookies for " +
                "certain activities on its site. What does Flipper’s policy " +
                "say regarding cookies?");
        question.setAnswers(Arrays.asList(
                new Answer(0, "Cookies cannot be blocked by browsers", false,
                        question),
                new Answer(1, "If you block cookies, you are not allowed to " +
                        "use the site", false, question),
                new Answer(2, "You can block cookies, but some aspects of " +
                        "Flipper might not work", true, question),
                new Answer(3, "You can block cookies and it will not affect " +
                        "your Flipper experience", false, question)
        ));
        questions.add(question);

        question = new Question(1, "Which of the following is NOT a type of " +
                "data that Flipper mentions in its policy?");
        question.setAnswers(Arrays.asList(
                new Answer(0, "Usage data", false, question),
                new Answer(1, "Data from cookies and tracking devices", false,
                        question),
                new Answer(2, "Personal data (i.e. name and email)", false,
                        question),
                new Answer(3, "Publicly available data from other sources",
                        true, question)
        ));
        questions.add(question);

        question = new Question(2, "All Flipper users must be in which of " +
                "the following age ranges?");
        question.setAnswers(Arrays.asList(
                new Answer(0, "13 and older", false, question),
                new Answer(1, "18 and older", true, question),
                new Answer(2, "21 and older", false, question),
                new Answer(3, "25 and older", false, question)
        ));
        questions.add(question);

        question = new Question(3, "If Flipper makes changes to its privacy " +
                "policy, it will");
        question.setAnswers(Arrays.asList(
                new Answer(0, "Contact you via email", true, question),
                new Answer(1, "Contact you and request your acceptance of " +
                        "the new policy", false, question),
                new Answer(2, "Contact those who have subscribed to a " +
                        "\"privacy interest\" list", false, question),
                new Answer(3, "Post the changes, but not contact you", false,
                        question)
        ));
        questions.add(question);

        question = new Question(4, "If someone too young to use Flipper " +
                "creates an account and Flipper learns about it, Flipper will");
        question.setAnswers(Arrays.asList(
                new Answer(0, "Take legal action", false, question),
                new Answer(1, "Take steps to delete their data", true,
                        question),
                new Answer(2, "Reprimand the user, but allow them to use the " +
                        "site", false, question),
                new Answer(3, "Contact their parents", false, question)
        ));
        questions.add(question);

        question = new Question(5, "Flipper may contain links to other " +
                "sites. In regards to these sites, Flipper");
        question.setAnswers(Arrays.asList(
                new Answer(0, "Requires that these sites adhere to this " +
                        "privacy policy", false, question),
                new Answer(1, "Renders the site inside Flipper so that no " +
                        "data goes to the sites", false, question),
                new Answer(2, "Makes no guarantees regarding the content " +
                        "and policies of the other site", true, question),
                new Answer(3, "Will not allow you to go to the site if its " +
                        "privacy policy does not meet Flipper standards", false,
                        question)
        ));
        questions.add(question);

        question = new Question(6, "Flipper says which of the following " +
                "regarding the security of your data?");
        question.setAnswers(Arrays.asList(
                new Answer(0, "Your data is always safe with us", false,
                        question),
                new Answer(1, "We’ll do our best, but we can’t guarantee " +
                        "it’s safe", true, question),
                new Answer(2, "The safety of your data depends on our " +
                        "partners, which don’t all have good privacy policies",
                        false, question),
                new Answer(3, "Flipper makes no statement regarding the " +
                        "security of your data", false, question)
        ));
        questions.add(question);

        question = new Question(7, "Flipper may need to release your data to " +
                "the government. Which of the following is not a situation " +
                "in which Flipper would release your data?");
        question.setAnswers(Arrays.asList(
                new Answer(0, "To comply with a government subpoena", false,
                        question),
                new Answer(1, "To protect themselves against liability", false,
                        question),
                new Answer(2, "To protect the personal safety of users", false,
                        question),
                new Answer(3, "To assist government employees making " +
                        "national security databases", true, question)
        ));
        questions.add(question);

        question = new Question(8, "Flipper may keep your data");
        question.setAnswers(Arrays.asList(
                new Answer(0, "Anywhere in the world", true, question),
                new Answer(1, "Only in your home country", false,
                        question),
                new Answer(2, "Only in Flipper’s home country", false,
                        question),
                new Answer(3, "Only in countries with strict data protection " +
                        "laws", false, question)
        ));
        questions.add(question);

        question = new Question(9, "What requirements does Flipper make " +
                "regarding sharing data with third party service providers?");
        question.setAnswers(Arrays.asList(
                new Answer(0, "Service providers get full access for their " +
                        "own use", false, question),
                new Answer(1, "Service providers may only use the data to " +
                        "provide services for Flipper", true, question),
                new Answer(2, "Flipper does not work with third party " +
                        "service providers", false, question),
                new Answer(3, "Flipper does not share data with third party " +
                        "service providers", false, question)
        ));
        questions.add(question);

        question = new Question(10, "On a scale of 1 - 10, how much do you " +
                "trust Flipper with your data now?");
        question.setAnswers(Arrays.asList(
                new Answer(0, "1", true, question),
                new Answer(1, "2", true, question),
                new Answer(2, "3", true, question),
                new Answer(3, "4", false, question),
                new Answer(4, "5", true, question),
                new Answer(5, "6", true, question),
                new Answer(6, "7", true, question),
                new Answer(7, "8", false, question),
                new Answer(8, "9", true, question),
                new Answer(9, "10", true, question)
        ));
        questions.add(question);

        return questions;
    }

    @Bean
    CommandLineRunner initDatabase(QuestionRepository repository) {
        return args -> {
            log.info("Preloading" +
                    repository.saveAll(this.addQuestionsAndAnswers()));
        };
    }
}
