package com.thesis.answers.infrastructure;

import com.thesis.answers.domain.AnswerDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAnswerRepository extends JpaRepository<AnswerDto, Integer> {
}
