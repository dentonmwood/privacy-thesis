package com.thesis.answers.domain;

import lombok.Data;

import javax.persistence.*;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="user")
public class User {
    @Id
    private Long id;
    private String ipAddress;
    private Integer trialNum;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "userAnswer", joinColumns = {
            @JoinColumn(name = "userId", nullable = false, updatable = false),
    }, inverseJoinColumns = {
            @JoinColumn(name = "answerId", nullable = false, updatable = false),
            @JoinColumn(name = "questionId", nullable = false, updatable =
                    false)
    })
    private List<Answer> answers;

    public User() {}

    public User(Long id, String ipAddress, Integer trialNum) {
        this.id = id;
        this.ipAddress = ipAddress;
        this.trialNum = trialNum;
        this.answers = new ArrayList<>();
    }
}
