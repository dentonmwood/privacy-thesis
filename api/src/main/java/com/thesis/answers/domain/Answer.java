package com.thesis.answers.domain;

import org.json.JSONObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="answer")
public class Answer {
    @Embeddable
    public static class AnswerPK implements Serializable {
        @Column(name = "id", nullable = false, updatable = false)
        private Integer id;
        @Column(name = "questionId", nullable = false, updatable = false)
        private Integer questionId;

        public AnswerPK() {}

        public AnswerPK(Integer id, Integer questionId) {
            this.id = id;
            this.questionId = questionId;
        }

        public Integer getId() {
            return this.id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getQuestionId() {
            return this.questionId;
        }

        public void setQuestionId(Integer questionId) {
            this.questionId = questionId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            AnswerPK answerPK = (AnswerPK) o;
            return this.id.equals(answerPK.getId()) &&
                    this.questionId.equals(answerPK.getQuestionId());
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.id, this.questionId);
        }

        @Override
        public String toString() {
            return "AnswerPK{" +
                    "id=" + id +
                    ", questionId=" + questionId +
                    '}';
        }
    }

    @EmbeddedId
    private AnswerPK id;
    @Column(name = "text")
    private String text;
    @Column(name = "correct")
    private boolean correct;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "questionId", insertable = false, updatable = false)
    private Question question;

    public Answer() {}

    public Answer(Integer id, String text, boolean correct, Question question) {
        this.id = new AnswerPK(id, question.getId());
        this.text = text;
        this.correct = correct;
        this.question = question;
    }

    public AnswerPK getId() {
        return this.id;
    }

    public void setId(AnswerPK id) {
        this.id = id;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isCorrect() {
        return this.correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public Question getQuestion() {
        // This is necessary to prevent an infinite loop of question & answer
        return null;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return this.correct == answer.isCorrect() &&
                this.id.equals(answer.getId()) &&
                this.text.equals(answer.getText());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.text, this.correct, this.question);
    }

    public String toString() {
        JSONObject info = new JSONObject();
        info.put("id", this.id);
        info.put("text", this.text);
        info.put("correct", this.correct);

        JSONObject questionInfo = new JSONObject();
        questionInfo.put("id", this.question.getId());
        questionInfo.put("text", this.question.getText());
        info.put("question", questionInfo);

        return info.toString();
    }
}
