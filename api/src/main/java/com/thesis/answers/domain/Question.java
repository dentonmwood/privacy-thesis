package com.thesis.answers.domain;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="question")
public class Question {
    @Id
    @Column(name = "id", nullable = false, updatable = false)
    private Integer id;

    @Column(name = "text")
    private String text;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "questionId")
    private List<Answer> answers;

    public Question() {}

    public Question(Integer id, String text) {
        this.id = id;
        this.text = text;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Answer> getAnswers() {
        return this.answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return this.id == question.getId() &&
                this.text.equals(question.getText()) &&
                this.answers.equals(question.getAnswers());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.text, this.answers);
    }

    public String toString() {
        JSONObject info = new JSONObject();
        info.put("id", this.id);
        info.put("text", this.text);

        JSONArray answersInfo = new JSONArray();
        if (this.answers != null) {
            this.answers.forEach(answer -> {
                JSONObject answerInfo = new JSONObject();
                answerInfo.put("id", answer.getId());
                answerInfo.put("text", answer.getText());
                answerInfo.put("correct", answer.isCorrect());
                answersInfo.put(answerInfo);
            });
        }
        info.put("answers", this.answers);
        return info.toString();
    }
}
