package com.thesis.answers.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;

@Data
@Entity
public class AnswerDto {
    @Id
    private Long id;
    private ArrayList<Integer> answers;

    public AnswerDto() {}

    public AnswerDto(Long id, ArrayList<Integer> answers) {
        this.id = id;
        this.answers = answers;
    }
}
