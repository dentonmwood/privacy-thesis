package com.thesis.answers.app;

import com.thesis.answers.app.exception.QuestionNotFoundException;
import com.thesis.answers.domain.Question;
import com.thesis.answers.infrastructure.QuestionRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class QuestionController {
    private QuestionRepository repository;

    public QuestionController(QuestionRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/questions")
    @CrossOrigin(origins = "http://localhost:4200")
    List<Question> all() {
        return repository.findAll();
    }
}
