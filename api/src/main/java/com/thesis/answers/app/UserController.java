package com.thesis.answers.app;

import com.thesis.answers.domain.Answer;
import com.thesis.answers.domain.Question;
import com.thesis.answers.domain.User;
import com.thesis.answers.domain.AnswerDto;
import com.thesis.answers.infrastructure.QuestionRepository;
import com.thesis.answers.infrastructure.UserAnswerRepository;
import com.thesis.answers.infrastructure.UserRepository;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
public class UserController {
    private UserRepository userRepository;
    private Random generator;
    private UserAnswerRepository answerRepository;
    private QuestionRepository questionRepository;

    private final int NUM_TRIALS = 4;

    public UserController(
            UserRepository repository,
            Random generator,
            UserAnswerRepository answerRepository,
            QuestionRepository questionRepository
    ) {
        this.userRepository = repository;
        this.generator = generator;
        this.answerRepository = answerRepository;
        this.questionRepository = questionRepository;
    }

    /*
    @GetMapping("/users")
    @CrossOrigin(origins = "http://localhost:4200")
    List<User> all() {
        return userRepository.findAll();
    }

    @GetMapping("/users/answers")
    @CrossOrigin(origins = "http://localhost:4200")
    List<AnswerDto> allAnswers() {
        return answerRepository.findAll();
    }
     */

    @GetMapping("/users/answers/data")
    Double[] allAnswerData() {
        List<AnswerDto> answers = answerRepository.findAll();
        List<Question> questions = questionRepository.findAll();
        Double[] proportionSums = new Double[NUM_TRIALS];
        int[] numParticipants = new int[NUM_TRIALS];
        // Don't include the opinion question
        int numQuestions = questions.size() - 1;

        for (int i = 0; i < NUM_TRIALS; i++) {
            proportionSums[i] = 0.0;
            numParticipants[i] = 0;
        }

        for (AnswerDto answer: answers) {
            int numCorrect = 0;
            ArrayList<Integer> answersGiven = answer.getAnswers();

            for (int i = 0; i < numQuestions; i++) {
                List<Answer> questionAnswers = questions.get(i).getAnswers();
                int answerGiven = answersGiven.get(i);
                if (questionAnswers.get(answerGiven).isCorrect()) {
                    numCorrect++;
                }
            }

            int trialNum = userRepository.getOne(answer.getId()).getTrialNum();
            proportionSums[trialNum] +=
                    (double) numCorrect / (double) numQuestions;
            numParticipants[trialNum]++;
        }

        Double[] proportions = new Double[NUM_TRIALS];
        for (int i = 0; i < NUM_TRIALS; i++) {
            proportions[i] = proportionSums[i] / (double) numParticipants[i];
        }
        return proportions;
    }

    @PostMapping("/users")
    @CrossOrigin(origins = "http://localhost:4200")
    User newUser(@RequestBody User user, HttpServletRequest request) throws UnknownHostException {
            user.setId(Math.abs(generator.nextLong()));
            user.setIpAddress(request.getRemoteAddr());
            user.setTrialNum(Math.abs(generator.nextInt(NUM_TRIALS)));
            user.setAnswers(new ArrayList<>());
            return userRepository.save(user);
    }

    @PostMapping(value = "/users/{id}", consumes = "application/json")
    @CrossOrigin(origins = "http://localhost:4200")
    AnswerDto replaceUser(
            @RequestBody AnswerDto answerDto,
            @PathVariable Long id
    ) {
        return answerRepository.save(answerDto);
    }
}
